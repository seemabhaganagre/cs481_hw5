﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Maps_HW5
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SatPage : ContentPage
    {


        public SatPage()
        {
            InitializeComponent();
            SMap.MapType = MapType.Satellite;

            SMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(37.801104, -122.426773), Distance.FromKilometers(5))); // initial location set to that coordinates


            var pin1 = new Pin
            {
                Position = new Position(37.801104, -122.426773), //pin1
                Label = "lombard street",      //pin name
                Address = "San Fransisco"     //pin address


            };
            var pin2 = new Pin
            {
                Position = new Position(37.826796, -122.423096), //pin2
                Label = "Alcatraz Island",              //pin name
                Address = "San Fransisco" //pin address

            };
            var pin3 = new Pin
            {
                Position = new Position(37.797397, -122.432520),    //pin 3
                Label = "Union Station",            //pin name
                Address = "San Fransisco"               //pin address


            };
            var pin4 = new Pin
            {
                Position = new Position(37.794608, -122.408498),        //pin 4
                Label = "China Town",         //pin name
                Address = "San Fransisco"              // pin address



            };
            var pin5 = new Pin
            {
                Position = new Position(37.808466, -122.414957),   //pin 5
                Label = "Madame Tussauds",                        //pin name
                Address = "San Fransisco"                      //pin address


            };

            ;
            SMap.Pins.Add(pin1);
            SMap.Pins.Add(pin2);
            SMap.Pins.Add(pin3);
            SMap.Pins.Add(pin4);
            SMap.Pins.Add(pin5);


        }
    }
}


